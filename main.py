import list_of_majors
import config
import time
import praw
import re


#Logging bot in
def bot_login():
    reddit = praw.Reddit(client_id = config.client_id,
                     client_secret = config.client_secret,
                     username = config.username,
                     password = config.password,
                     user_agent = config.user_agent,)
    print('Logged in!')
    return reddit
# Initializing bot program...
def run_bot(reddit):
    phrases = list_of_majors.majorFiles.keys()
    # Bot is active in these subreddits
    subreddit = reddit.subreddit('test')
    # Searching for commands
    try:
        for comment in subreddit.comments():
            #Bot prints a list of the majors
            if re.search('!majors', comment.body, re.IGNORECASE):
                with open('./TextFiles/CollegeMajors.txt', "r") as majors:
                    comment.reply(majors.read())
    except:
        print('That didn\'t work...')
    #Sleep for a minute -- Temporary for testing
    print('Sleeping for one minute')
    time.sleep(10)


reddit = bot_login()
while True:
    run_bot(reddit)
